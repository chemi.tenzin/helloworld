package hello;

import org.junit.Assert;
import org.junit.Test;

public class BookTest {

    @Test
    public void shouldCreateAnAppWithName() {
        Book app = new Book("Hello");
        Assert.assertNotNull(app);
    }
}
