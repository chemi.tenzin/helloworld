package hello;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(HelloController.class)
public class AppIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void applicationShouldReturnBookJson() throws Exception {
        this.mockMvc.perform(get("/application")).andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Book Shop"));
    }
}