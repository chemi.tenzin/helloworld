package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping(path = "/application")
    public Book sayHello()
    {
        return new Book("Book Shop");
    }

}